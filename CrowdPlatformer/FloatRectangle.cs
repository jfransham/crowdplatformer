﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace CrowdPlatformer
{
    public struct FloatRectangle
    {
        public float X,
            Y,
            Width,
            Height;

        public Vector2 Position { get { return new Vector2(X, Y); } }
        public float Left { get { return X; } }
        public float Right { get { return X + Width; } }
        public float Top { get { return Y; } }
        public float Bottom { get { return Y + Height; } }

        public FloatRectangle(float x, float y, float w, float h)
        {
            X = x;
            Y = y;
            Width = w;
            Height = h;
        }

        public static bool Intersects(FloatRectangle a, FloatRectangle b)
        {
            return
                (a.X.Between(b.Left, b.Right) && a.Y.Between(b.Top, b.Bottom)) ||
                (b.X.Between(a.Left, a.Right) && b.Y.Between(a.Top, a.Bottom));
        }
    }
}
