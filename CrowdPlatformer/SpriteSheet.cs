﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CrowdPlatformer
{
    public class SpriteSheet
        : DisplayObject
    {
        public readonly Texture2D Source;
        public readonly int Width,
            Height;
        public int Frame;
        public readonly int TotalFrames;

        public SpriteSheet(Texture2D t, int w, int h)
        {
            Source = t;
            Width = w;
            Height = h;
            TotalFrames = (Source.Width*Source.Height)/(Width*Height);
        }

        public override void Render(SpriteBatch s, GraphicsDevice g, Matrix transMatrix, GameTime dt)
        {
            Matrix m = transMatrix*GetTransformationMatrix();

            int yOffset = (Frame/(Source.Width/Width))*Height;
            int xOffset = (Width*Frame) % Source.Width;
            s.Draw(Source, Vector2.Transform(Vector2.One, m), new Rectangle(xOffset, yOffset, Width, Height), Color, 0, Origin,
                Scale, Flipped ? SpriteEffects.FlipHorizontally : SpriteEffects.None, 0);
        }
    }
}
