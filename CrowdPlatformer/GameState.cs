﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CrowdPlatformer
{
    public class GameState
        : DisplayObjectContainer
    {
        public Tilemap Tilemap;

        public override void UpdateInternal(GameTime dt)
        {
            base.UpdateInternal(dt);

            for (int i = 0; i < Children.Count; i++)
            {
                DisplayObject a = Children[i];

                for (int j = i + 1; j < Children.Count; j++)
                {
                    DisplayObject b = Children[j];

                    if (CollisionShape.Collides(a.Collider, b.Collider))
                    {
                        a.DoBump(b);
                        b.DoBump(a);
                    }
                }

                if (Tilemap == null)
                    continue;

                bool c;
                Vector2 norm;
                do
                {
                    c = Tilemap.Collides(a.Collider, a.Position, out norm);

                    if (!c || norm.LengthSquared() == 0)
                        break;

                    norm.Normalize();
                } while (a.WorldCollide(norm));
            }
        }

        public override void Render(SpriteBatch s, GraphicsDevice g, Matrix transMatrix, GameTime dt)
        {
            base.Render(s, g, transMatrix, dt);
            
            if (Tilemap != null)
                Tilemap.Render(s, g, transMatrix, dt);
        }   
    }
}
