﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace CrowdPlatformer
{
    public class Player
        : DisplayObject
    {
        public const float MaxAcc = 400;
        public const float MaxVelocity = 200;
        public const float Friction = 700;
        public const float Gravity = 700;
        public const float JumpSpeed = 300;
        public const float PopSpeed = 60;
        public const float PopGravity = 650;
        public const int MaxJumpWaitFrames = 4;
        public const int MaxStillPopWait = 240;
        public const int MinStillPopWait = 30;
        public const int MaxMovingPopWait = 30;
        public const int MinMovingPopWait = 0;

        private float _popHeight = 0;
        private float _popVel = 0;
        private int _jumpCountdown;
        private int _popCountdown;

        public readonly AnimatedSpriteSheet SpriteSheet;
        public float Acc = MaxAcc;
        public Vector2 Velocity = Vector2.Zero;
        public float SpeedScaling = 1;
        public float ChangeDirThreshold = 0.4f;
        public int MoveDir { get; set; }
        public Func<bool> ShouldJump = null; 

        public Player()
        {
            SpriteSheet = new AnimatedSpriteSheet(StaticGame.Content.Load<Texture2D>("player.png"), 20, 20);
            SpriteSheet.AddAnimation("walk", TimeSpan.FromSeconds(0.1),
                1, 2);
            SpriteSheet.AddAnimation("idle", TimeSpan.MaxValue,
                0);
            SpriteSheet.SetAnimation("idle", TimeSpan.Zero);
            SpriteSheet.Origin = new Vector2(10, 20);
            SpriteSheet.Scale = (float) (0.14*StaticGame.Random.NextDouble() - 0.07) + 1;
            SpriteSheet.Position.Y = 0;
            SpriteSheet.Position.X = 0;
            Collider = CollisionShape.Ellipse(10, 10, -Vector2.UnitY*10);
        }

        public void MoveRight()
        {
            MoveDir = 1;
        }

        public void MoveLeft()
        {
            MoveDir = -1;
        }

        public void StopMoving()
        {
            MoveDir = 0;
        }

        public void Jump()
        {
            if (_jumpCountdown >= 0 || !CheckJump())
                return;

            _jumpCountdown = StaticGame.Random.Next(MaxJumpWaitFrames);
        }

        public bool CanJump()
        {
            return Position.Y >= 300;
        }

        public bool CheckJump()
        {
            return ShouldJump == null || ShouldJump();
        }

        private void DoJump()
        {
            if (!CanJump())
                return;

            Velocity.Y = -JumpSpeed;
        }

        private bool MovingInDesiredDirection { get { return MoveDir != 0 &&
            ((Velocity.X < 0 && MoveDir < 0) ||
            (Velocity.X > 0 && MoveDir > 0)); } }
        private float UseAcceleration { get { return MovingInDesiredDirection ? SpeedScaling*Acc : Friction; } }

        public override void Update(GameTime dt)
        {
            if (_jumpCountdown == 0)
                DoJump();
            
            if (_jumpCountdown >= 0)
                _jumpCountdown--;

            if (MoveDir != 0)
            {
                if (SpriteSheet.CurrentAnimation != "walk") 
                    SpriteSheet.SetAnimation("walk", dt.TotalGameTime);
            }
            else
            {
                SpriteSheet.SetAnimation("idle", dt.TotalGameTime);
            }

            SpriteSheet.Update(dt);
            float actualAcc = UseAcceleration*(float) dt.ElapsedGameTime.TotalSeconds;
            if (MoveDir == 0)
            {
                if (Math.Abs(Velocity.X) < Math.Abs(actualAcc))
                    Velocity.X = 0;
                else
                    Velocity.X = Math.Sign(Velocity.X)*(Math.Abs(Velocity.X) - Math.Abs(actualAcc));
            }
            else
            {
                Velocity.X += MoveDir * actualAcc;
                Velocity.X = Math.Sign(Velocity.X) * Math.Min(Math.Abs(Velocity.X), MaxVelocity * SpeedScaling);
                if (Velocity.X * MoveDir > MaxVelocity * ChangeDirThreshold)
                    SpriteSheet.Flipped = (MoveDir == -1);
            }

            Velocity.Y += Gravity*(float)dt.ElapsedGameTime.TotalSeconds;
            _popVel += PopGravity*(float) dt.ElapsedGameTime.TotalSeconds;
            _popHeight = Math.Min(_popHeight + _popVel*(float) dt.ElapsedGameTime.TotalSeconds, 0);

            if (Position.Y >= 300)
            {
                Velocity.Y = Math.Min(Velocity.Y, 0);
                Position.Y = 300;

                if (_popHeight == 0 && _popCountdown == 0)
                {
                    _popVel = -PopSpeed;
                    _popCountdown = MoveDir == 0 ?
                        StaticGame.Random.Next(MinStillPopWait, MaxStillPopWait) :
                        StaticGame.Random.Next(MinMovingPopWait, MaxMovingPopWait);
                }

                if (_popCountdown > 0)
                    _popCountdown--;
                else
                    _popCountdown = 0;
            }
            else
            {
                _popHeight = 
                    _popVel = 0;
            }

            SpriteSheet.Position.Y = SpriteSheet.Height + _popHeight;

            Position += Velocity*(float)dt.ElapsedGameTime.TotalSeconds;
        }

        public override bool WorldCollide(Vector2 normal)
        {
            Velocity = Velocity.Project(new Vector2(normal.Y, normal.X));
            Position -= normal;
            return true;
        }

        public override void Render(SpriteBatch s, GraphicsDevice g, Matrix transMatrix, GameTime dt)
        {
            SpriteSheet.Render(s, g, transMatrix * GetTransformationMatrix(), dt);
        }
    }
}
