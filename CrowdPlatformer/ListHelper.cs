﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CrowdPlatformer
{
    public static class ListHelper
    {
        public static void Shuffle<T>(this IList<T> list, Random r = null)
        {
            Random rng = r ?? new Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static float GetRepeatValueArrayElement(Tuple<int, float>[] arr, int index)
        {
            int i = 0;
            while (index > 0 && i < arr.Length - 1)
            {
                index -= arr[i].Item1;
                i++;
            }

            return arr[i].Item2;
        }

        public static List<int> GetIndices(int length)
        {
            List<int> output = new List<int>(length);
            for (int i = 0; i < length; i++)
                output.Add(i);
            return output;
        }
    }
}
