﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CrowdPlatformer
{
    public abstract class DisplayObject
    {
        public Vector2 Position;
        public float Scale = 1.0f;
        public Vector2 Origin = Vector2.Zero;
        public bool Flipped = false;
        public CollisionShape Collider;
        public Color Color = Color.White;

        private DisplayObjectContainer _parent;
        public DisplayObjectContainer Parent
        {
            get { return _parent; }
            set
            {
                DisplayObjectContainer old = _parent;
                _parent = value;

                if (old == Parent)
                    return;
                
                if (old != null && Removed != null)
                    Removed.Invoke(old);
                if (Parent != null && Added != null)
                    Added.Invoke(Parent);
            }
        }

        public Matrix GetTransformationMatrix()
        {
            return Matrix.CreateTranslation(Position.X, Position.Y, 0);
        }

        public virtual void Update(GameTime dt) {}
        public virtual void UpdateInternal(GameTime dt) { Update(dt); }
        public abstract void Render(SpriteBatch s, GraphicsDevice g, Matrix transMatrix, GameTime dt);

        public void DoBump(DisplayObject other)
        {
            if (Bump == null)
                return;

            Bump.Invoke(other);
        }

        public virtual bool WorldCollide(Vector2 normal) { return false; }

        public event Action<DisplayObjectContainer> Added;
        public event Action<DisplayObjectContainer> Removed;
        public event Action<DisplayObject> Bump;
    }
}
