﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using OpenTK.Platform.Windows;

namespace CrowdPlatformer
{
    public static class GameMaths
    {
        public static float Lerp(float from, float to, float perc)
        {
            return from + (to - from)*perc.Clamp(0, 1);
        }

        public static bool Between(this float a, float min, float max)
        {
            return a > min && a < max;
        }

        public static float Clamp(this float a, float min, float max)
        {
            return a < min ? min : (a > max ? max : a);
        }

        public static Vector2 Project(this Vector2 vec, Vector2 onto)
        {
            onto.Normalize();
            float dist = Vector2.Dot(vec, onto);
            return dist*onto;
        }

        public static float SegmentDistance(Vector2 a1, Vector2 a2, Vector2 b1, Vector2 b2, out Vector2 closestA, out Vector2 closestB)
        {
            Vector2 u = a1 - a2;
            Vector2 v = b1 - b2;
            Vector2 w = a2 - b2;

            float a = Vector2.Dot(u, u);
            float b = Vector2.Dot(u, v);
            float c = Vector2.Dot(v, v);
            float d = Vector2.Dot(u, w);
            float e = Vector2.Dot(v, w);
            float D = a*c - b*b;
            float sD = D;
            float tD = D;
            float sN,
                tN;

            const float smallNum = 0.00000001f;

            // compute the line parameters of the two closest points
            if (D < smallNum) // the lines are almost parallel
            {
                sN = 0.0f; // force using point P0 on segment S1
                sD = 1.0f; // to prevent possible division by 0.0 later
                tN = e;
                tD = c;
            }
            else // get the closest points on the infinite lines
            {
                sN = (b*e - c*d);
                tN = (a*e - b*d);
            }

            if (sN < 0.0) // sc < 0 => the s=0 edge is visible       
            {
                sN = 0.0f;
                tN = e;
                tD = c;
            }
            else if (sN > sD)
            {
                // sc > 1 => the s=1 edge is visible
                sN = sD;
                tN = e + b;
                tD = c;
            }

            if (tN < 0.0) // tc < 0 => the t=0 edge is visible
            {
                tN = 0.0f;
                // recompute sc for this edge
                if (-d < 0.0)
                {
                    sN = 0.0f;
                }
                else if (-d > a)
                {
                    sN = sD;
                }
                else
                {
                    sN = -d;
                    sD = a;
                }
            }
            else if (tN > tD) // tc > 1 => the t=1 edge is visible
            {
                tN = tD;
            }

            // recompute sc for this edge
            if ((-d + b) < 0.0)
            {
                sN = 0;
            }
            else if ((-d + b) > a)
            {
                sN = sD;
            }
            else
            {
                sN = (-d + b);
                sD = a;
            }

            float sc, tc;
            // finally do the division to get sc and tc
            if(Math.Abs(sN) < smallNum)
                sc = 0.0f;
            else
                sc = sN / sD;
    
            if(Math.Abs(tN) < smallNum)
                tc = 0.0f;
            else
                tc = tN / tD;
    
            // get the difference of the two closest points
            Vector2 dP = w + (sc * u) - (tc * v);  // = S1(sc) - S2(tc)

            closestA = a2+sc*u;
            closestB = b2+tc*v;

            return dP.Length();
        }

        public static float MinimumDistance(Vector2 p, Vector2 v, Vector2 w)
        {
            bool a;
            return MinimumDistance(p, v, w, out a);
        }

        public static float MinimumDistance(Vector2 p, Vector2 v, Vector2 w, out bool withinSegment)
        {
            withinSegment = true;

            // Return minimum distance between line segment vw and point p
            float l2 = (v - w).LengthSquared();  // i.e. |w-v|^2 -  avoid a sqrt
            if (l2 == 0.0)
                return (p - v).Length();   // v == w case

            // Consider the line extending the segment, parameterized as v + t (w - v).
            // We find projection of point p onto the line. 
            // It falls where t = [(p-v) . (w-v)] / |w-v|^2

            float t = Vector2.Dot(p - v, w - v) / l2;

            if (t < 0.0 || t > 1.0)
                withinSegment = false;

            Vector2 projection = v + t * (w - v);  // Projection falls on the segment

            return (p - projection).Length();
        }
    }
}
