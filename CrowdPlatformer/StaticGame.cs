﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;

namespace CrowdPlatformer
{
    public static class StaticGame
    {
        public static ContentManager Content;
        public static readonly Random Random = new Random();
        public static string DebugReadout { get; set; }
    }
}
