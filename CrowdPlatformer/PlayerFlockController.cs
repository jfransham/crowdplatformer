﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace CrowdPlatformer
{
    public class PlayerFlockController
        : DisplayObject
    {
        private List<int> _indices;
        public List<Player> Players;

        public float MovingWidth { get { return ListHelper.GetRepeatValueArrayElement(MovingWidths, Players.Count - 1); } }
        public static readonly Tuple<int, float>[] MovingWidths =   { new Tuple<int, float>(1, float.Epsilon),
                                                                      new Tuple<int, float>(10, 30),
                                                                      new Tuple<int, float>(20, 70),
                                                                      new Tuple<int, float>(120, 120), 
                                                                      new Tuple<int, float>(100, 300),
                                                                      new Tuple<int, float>(100, 800) };
        
        public float StillWidth { get { return ListHelper.GetRepeatValueArrayElement(StillWidths, Players.Count - 1); } }
        public static readonly Tuple<int, float>[] StillWidths =    { new Tuple<int, float>(1, float.Epsilon),
                                                                      new Tuple<int, float>(10, 25),
                                                                      new Tuple<int, float>(20, 30),
                                                                      new Tuple<int, float>(120, 50), 
                                                                      new Tuple<int, float>(100, 150),
                                                                      new Tuple<int, float>(100, 400) };

        public float JumpWidth { get { return ListHelper.GetRepeatValueArrayElement(JumpWidths, Players.Count - 1); } }
        public static readonly Tuple<int, float>[] JumpWidths =     { new Tuple<int, float>(1, float.Epsilon),
                                                      new Tuple<int, float>(4, 10), 
                                                      new Tuple<int, float>(20, 20), 
                                                      new Tuple<int, float>(1, 30) };
        
        public float Elasticity = 0.01f;
        public float LerpSpeed = 10.0f;
        public float AirLerpSpeed = 20000000.0f;
        public float OutsideBoundsElasticityScale = 10.0f;
        public float AirElasticity = 20.0f;
        public float MinDistance = 2.0f;
        public bool MovingLeft = false;
        public int LastDir = 0;
        public bool AllOnFloor = false;
        public TimeSpan ChangePositionTime = TimeSpan.FromSeconds(2);

        public float GetOffsetFromIndex(int index, float w, Random r)
        {
            const float randAmount = 0.6f;
            const float distPower = 0.0f;
            
            float step = w / Players.Count;
            float offset = step * (index + 0.5f) - ((float)r.NextDouble() * step * randAmount - randAmount * step / 2);

            /** BROKEN CODE: attempting to shift characters in direction of travel **/
            //const float middlePerc = 1.0f;
            //float leftWidth = MovingLeft ? MovingWidth * (1 - middlePerc) : MovingWidth * middlePerc;
            //float rightWidth = MovingWidth - leftWidth;
            //float dif = offset - leftWidth;

            //float perc = Math.Abs(dif) / (dif < 0 ? leftWidth : rightWidth);

            float perc = 2 * Math.Abs(offset - w / 2) / w;

            return (offset - w/2)*(float)Math.Pow(perc, distPower) + w/2;
        }

        public float GetWidth(int dir, bool allOnFloor)
        {
            return allOnFloor ? (dir == 0 ? StillWidth : MovingWidth) : JumpWidth;
        }

        public PlayerFlockController(List<Player> players)
        {
            Players = new List<Player>(players.Count);

            foreach (Player p in players)
                AddPlayer(p);

            Added += par =>
            {
                foreach (Player p in Players)
                    par.AddChild(p);
            };

            Removed += par =>
            {
                foreach (Player p in Players)
                    par.RemoveChild(p);
            };
        }

        public void AddPlayer(Player p)
        {
            Players.Add(p);

            p.ShouldJump = () => AllOnFloor;
            p.Acc = Player.MaxAcc * (1 - 0.05f * (float)StaticGame.Random.NextDouble());
            p.ChangeDirThreshold = 0.4f + ((float)StaticGame.Random.NextDouble() * 0.4f - 0.2f);

            if (Parent != null)
                Parent.AddChild(p);
        }

        public override void Update(GameTime dt)
        {
            float avrX = 0;
            float highestX = float.NegativeInfinity;
            float lowestX = float.PositiveInfinity;
            AllOnFloor = true;
            foreach (Player p in Players)
            {
                AllOnFloor &= p.CanJump();
                avrX += p.Position.X;
                highestX = Math.Max(highestX, p.Position.X);
                lowestX = Math.Min(lowestX, p.Position.X);
            }
            avrX /= Players.Count;

            int dir = 0;
            bool jump = false;
            GamePadState s = GamePad.GetState(PlayerIndex.One);
            KeyboardState k = Keyboard.GetState();

            if (s.DPad.Left == ButtonState.Pressed || s.ThumbSticks.Left.X < 0 || k.IsKeyDown(Keys.Left) ||
                k.IsKeyDown(Keys.A))
            {
                dir -= 1;
                MovingLeft = true;
            }
            if (s.DPad.Right == ButtonState.Pressed || s.ThumbSticks.Left.X > 0 || k.IsKeyDown(Keys.Right) ||
                k.IsKeyDown(Keys.D))
            {
                dir += 1;
                MovingLeft = false;
            }
            if (s.IsButtonDown(Buttons.A) || k.IsKeyDown(Keys.Space))
            {
                jump = true;
            }

            float width = GetWidth(dir, AllOnFloor);
            float leftSide = MovingLeft ? lowestX : highestX - width;
            float rightSide = MovingLeft ? lowestX + width : highestX;

            if (_indices == null || _indices.Count != Players.Count || dir != LastDir)
            {
                _indices = ListHelper.GetIndices(Players.Count);
                _indices.Shuffle();
            }

            LastDir = dir;

            Random r = new Random(Players.Count);
            for(int i = 0; i < Players.Count; i++)
            {
                Player p = Players[i];
                if (jump)
                    p.Jump();

                int index = _indices[i];

                float offset = GetOffsetFromIndex(index, width, r);

                float pos = leftSide + offset;

                if (dir == 0)
                {
                    if (p.Position.X < leftSide)
                        p.MoveRight();
                    else if (p.Position.X > rightSide)
                        p.MoveLeft();
                    else
                        p.StopMoving();
                }
                else
                {
                    p.MoveDir = dir;
                }

                float useE = AllOnFloor ? Elasticity : AirElasticity;
                float elasticityScale = (p.Position.X < leftSide || p.Position.X > rightSide)
                    ? OutsideBoundsElasticityScale
                    : 1;
                float anchor = p.Position.X < leftSide
                                    ? leftSide
                                    : (p.Position.X > rightSide
                                        ? rightSide
                                        : pos);
                float distance = Math.Abs(p.Position.X - anchor);
                if (distance <= MinDistance)
                    distance = 0;
                float scale = 1 - (1 / (1 + useE * elasticityScale * distance));
                float clampedUpScale = scale < 0.01f ? 0 : scale;
                float clampedDownScale = scale < 0.01f ? 0 : scale;
                if (!AllOnFloor)
                    clampedDownScale = 0;

                float l = AllOnFloor ? LerpSpeed : AirLerpSpeed;
                if (dir < 0)
                    p.SpeedScaling = GameMaths.Lerp(p.SpeedScaling, p.Position.X <= anchor ? 1 - clampedDownScale : 1 + clampedUpScale, l * (float)dt.ElapsedGameTime.TotalSeconds);
                else if (dir > 0)
                    p.SpeedScaling = GameMaths.Lerp(p.SpeedScaling, p.Position.X >= anchor ? 1 - clampedDownScale : 1 + clampedUpScale, l * (float)dt.ElapsedGameTime.TotalSeconds);
                else
                    p.SpeedScaling = 1;// + scale;
            }
        }

        public override void Render(SpriteBatch s, GraphicsDevice g, Matrix transMatrix, GameTime dt)
        {
        }
    }
}
