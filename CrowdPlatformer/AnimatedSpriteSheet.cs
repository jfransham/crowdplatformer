﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CrowdPlatformer
{
    public struct Animation
    {
        public readonly TimeSpan FrameLength;
        public readonly int[] Frames;

        public Animation(TimeSpan t, int[] f)
        {
            FrameLength = t;
            Frames = f;
        }
    }

    public class AnimatedSpriteSheet
        : SpriteSheet
    {
        public readonly Dictionary<string, Animation> Animations = new Dictionary<string, Animation>();
        public string CurrentAnimation { get; private set; }
        public TimeSpan StartedAt { get; private set; }

        public override void Update(GameTime dt)
        {
            if (CurrentAnimation == null)
                return;

            Frame = (int)((dt.TotalGameTime - StartedAt).Ticks/Animations[CurrentAnimation].FrameLength.Ticks);
            Frame %= Animations[CurrentAnimation].Frames.Length;
        }

        public void SetAnimation(string animation, TimeSpan time)
        {
            CurrentAnimation = animation;
            StartedAt = time;
        }

        public void AddAnimation(string name, TimeSpan t, params int[] frames)
        {
            Animations.Add(name, new Animation(t, frames));
        }

        public AnimatedSpriteSheet(Texture2D t, int w, int h) : base(t, w, h)
        {
        }
    }
}
