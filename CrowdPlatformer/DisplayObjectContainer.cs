﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CrowdPlatformer
{
    public class DisplayObjectContainer
        : DisplayObject
    {
        public readonly List<DisplayObject> Children = new List<DisplayObject>();

        public void AddChild(DisplayObject d)
        {
            if (d == null || Children.Contains(d))
                return;

            Children.Add(d);
            d.Parent = this;
        }

        public void RemoveChild(DisplayObject d)
        {
            if (d == null || !Children.Contains(d))
                return;

            Children.Remove(d);

            if (d.Parent == this)
                d.Parent = null;
        }

        public override void UpdateInternal(GameTime dt)
        {
            base.UpdateInternal(dt);

            foreach (DisplayObject displayObject in Children)
                displayObject.UpdateInternal(dt);
        }

        public override void Render(SpriteBatch s, GraphicsDevice g, Matrix transMatrix, GameTime dt)
        {
            Matrix m = transMatrix * GetTransformationMatrix();

            foreach (DisplayObject displayObject in Children)
                displayObject.Render(s, g, m, dt);
        }
    }
}
