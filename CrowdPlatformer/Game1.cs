﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
//using Microsoft.Xna.Framework.GamerServices;
#endregion

namespace CrowdPlatformer
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        AnimatedSpriteSheet tiles;
        private DisplayObjectContainer container;
        private Tilemap t;

        public Game1()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            StaticGame.Content = Content;
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            container = new GameState();
            List<Player> players = new List<Player>();
            Random r = new Random();
            for (int i = 0; i < 1; i++)
            {
                Player plyr = new Player();
                plyr.Position = new Vector2(300 + (float)r.NextDouble() * 100f, 0);
                players.Add(plyr);
            }
            PlayerFlockController p = new PlayerFlockController(players);
            SpriteSheet tilem = new SpriteSheet(Content.Load<Texture2D>("tiles.png"), 32, 32);
            t = new Tilemap(tilem, File.ReadAllText("Content/map.csv"), 1);
            container.AddChild(p);

            IsMouseVisible = true;

            //for (int x = 0; x < t.SpriteSheet.Source.Width; x+=5)
            //    for (int y = 0; y < t.SpriteSheet.Source.Height; y+=5)
            //        container.AddChild(new Player{Position = new Vector2(x, y)});

            //(container as GameState).Tilemap = t;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            //gameTime.ElapsedGameTime = new TimeSpan(gameTime.ElapsedGameTime.Ticks / 2);

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            if (Keyboard.GetState().IsKeyDown(Keys.L))
            {
                Player p = new Player();
                (container.Children[0] as PlayerFlockController).AddPlayer(p);
                //container.AddChild(p);
            }

            container.UpdateInternal(gameTime);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            PlayerFlockController pc = (container.Children[0] as PlayerFlockController);
            float avrX = pc.Players.Sum(p => p.Position.X) / pc.Players.Count;

            spriteBatch.Begin(SpriteSortMode.Immediate,
                BlendState.AlphaBlend,
                SamplerState.PointWrap,
                DepthStencilState.Default,
                RasterizerState.CullNone,
                null,
                Matrix.CreateTranslation(GraphicsDevice.Viewport.Width / 2 - avrX, 0, 0));
            container.Render(spriteBatch, GraphicsDevice, Matrix.Identity, gameTime);
            //spriteBatch.DrawString(Content.Load<SpriteFont>("test"), StaticGame.DebugReadout, Vector2.Zero, Color.Red);
            spriteBatch.End();
            
            base.Draw(gameTime);
        }
    }
}
