﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CrowdPlatformer
{
    public class Tilemap
        : DisplayObject
    {
        public static bool NoCollider(Vector2 v) { return false; }
        public static bool SquareCollider(Vector2 v) { return true; }
        
        public static Predicate<Vector2> RampCollider(float yOffset, float rad)
        {
            return MatrixCollider(Matrix.CreateTranslation(0, yOffset, 0) * Matrix.CreateRotationZ(rad));
        }

        public static Predicate<Vector2> MatrixCollider(Matrix translation)
        {
            return v => Vector2.Transform(v, translation).Y >= 0.5f;
        }

        private static int[,] Parse(string csv)
        {
            string[][] ints =   csv.Split('\n').Select(
                                    s =>
                                        s.Split(',').Where(
                                            e =>
                                                Regex.IsMatch(e, @"\d+")
                                        ).ToArray()
                                ).Where(s => s != null && s.Length > 0).ToArray();

            int[,] tls = new int[ints[0].Count(), ints.Length];
            for (int x = 0; x < tls.GetLength(0); x++)
                for (int y = 0; y < tls.GetLength(1); y++)
                    tls[x, y] = int.Parse(ints[y][x]);

            return tls;
        }

        private static Predicate<Vector2>[] StandardColliders(int collideIndex, int totalFrames)
        {
            Predicate<Vector2>[] output = new Predicate<Vector2>[totalFrames];

            for (int i = 0; i < totalFrames; i++)
                output[i] = i < collideIndex ? (Predicate<Vector2>)NoCollider : SquareCollider;

            return output;
        }

        public RenderTarget2D CachedTilemap { get; private set; }
        public readonly SpriteSheet SpriteSheet;
        public readonly int[,] Tiles;
        public readonly Predicate<Vector2>[] TileColliders;

        public Tilemap(SpriteSheet sprites, string csv, int cIndex = 0)
            : this(sprites, csv, StandardColliders(cIndex, sprites.TotalFrames))
        {
        }

        public Tilemap(SpriteSheet sprites, string csv, Predicate<Vector2>[] colliders)
            : this(sprites, Parse(csv), colliders)
        { 
        }

        public Tilemap(SpriteSheet sprites, int[,] tiles, Predicate<Vector2>[] colliders)
        {
            SpriteSheet = sprites;
            TileColliders = colliders;
            Tiles = tiles;
            CachedTilemap = null;
        }

        public bool Collides(CollisionShape c, Vector2 offset, out Vector2 collisionDir)
        {
            if (c == null)
            {
                collisionDir = Vector2.Zero;
                return false;
            }

            Vector2 avr = Vector2.Zero;
            int numC = 0;

            foreach (var p in c.Points)
            {
                if (Collides(p + offset))
                {
                    avr += p;
                    numC++;
                }
            }

            if (numC == 0)
            {
                collisionDir = Vector2.Zero;
                return false;
            }

            collisionDir = avr/numC;
            
            return true;
        }

        public bool Collides(Vector2 v)
        {
            Vector2 test = new Vector2(v.X / SpriteSheet.Width, v.Y / SpriteSheet.Height);
            Point cell = new Point((int)Math.Floor(test.X), (int)Math.Floor(test.Y));

            if (cell.X < 0 || cell.Y < 0 ||
                cell.X >= Tiles.GetLength(0) ||
                cell.Y >= Tiles.GetLength(1))
                return false;

            Vector2 innerCell = new Vector2(test.X - cell.X, test.Y - cell.Y);

            return TileColliders[Tiles[cell.X, cell.Y]](innerCell);
        }

        public override void Render(SpriteBatch s, GraphicsDevice g, Matrix transMatrix, GameTime dt)
        {
            if (CachedTilemap != null)
                s.Draw(CachedTilemap, Position, Color.White);

            for (int x = 0; x < Tiles.GetLength(0); x++)
            {
                for (int y = 0; y < Tiles.GetLength(1); y++)
                {
                    if (Tiles[x, y] == 0)
                        continue;
                    
                    SpriteSheet.Position = new Vector2(SpriteSheet.Width*x, SpriteSheet.Height*y);
                    SpriteSheet.Frame = Tiles[x, y];
                    SpriteSheet.Render(s, g, transMatrix, dt);
                }
            }
        }
    }
}
