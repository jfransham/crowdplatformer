﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace CrowdPlatformer
{
    public class CollisionShape
    {
        public readonly Vector2[] Points;
        public readonly FloatRectangle AABB;

        public CollisionShape(Vector2[] p)
        {
            Points = p;
            float minX = float.PositiveInfinity,
                minY = float.PositiveInfinity,
                maxX = float.NegativeInfinity,
                maxY = float.NegativeInfinity;
            foreach (Vector2 point in Points)
            {
                if (point.X < minX)
                    minX = point.X;
                if (point.X > maxX)
                    maxX = point.X;
                if (point.Y < minY)
                    minY = point.Y;
                if (point.Y > maxY)
                    maxY = point.Y;
            }
            AABB = new FloatRectangle();
        }

        public static bool Collides(CollisionShape a, CollisionShape b)
        {
            return false;
        }

        /// <summary>
        /// OUTDATED
        /// </summary>
        public static bool Collides_OUTDATED(CollisionShape a, CollisionShape b)
        {
            if (!FloatRectangle.Intersects(a.AABB, b.AABB))
                return false;

            float minDistance = float.PositiveInfinity;
            Vector2 perpendicularAxis = Vector2.Zero;

            for (int i = 0; i < a.Points.Length; i++)
            {
                int nexti = (i + 1)%a.Points.Length;

                for (int j = 0; j < b.Points.Length; j++)
                {
                    int nextj = (j + 1)%b.Points.Length;

                    Vector2 cA, cB;
                    Vector2 a0 = a.Points[i],
                        a1 = a.Points[nexti],
                        b0 = b.Points[j],
                        b1 = b.Points[nextj];
                    float dist = GameMaths.SegmentDistance(a0, a1, b0, b1, out cA, out cB);

                    if ((cA == a0 || cA == a1) &&
                        (cB == b0 || cB == b1))
                        continue;

                    if (dist < minDistance)
                    {
                        minDistance = dist;
                        perpendicularAxis = (cA == a0 || cA == a1)
                            ? b0 - b1
                            : a0 - a1;
                    }
                }
            }

            if (perpendicularAxis.LengthSquared() == 0)
                return false;

            float rads = (float)Math.PI/2 - (float)Math.Atan2(perpendicularAxis.Y, perpendicularAxis.X);

            Matrix trans = Matrix.CreateRotationZ(rads);

            float minA = float.PositiveInfinity,
                maxA = float.NegativeInfinity,
                minB = float.PositiveInfinity,
                maxB = float.NegativeInfinity;

            foreach (Vector2 p in a.Points)
            {
                Vector2 t = Vector2.Transform(p, trans);
                if (t.X < minA)
                    minA = t.X;
                if (t.X > maxA)
                    maxA = t.X;
            }

            foreach (Vector2 p in b.Points)
            {
                Vector2 t = Vector2.Transform(p, trans);
                if (t.X < minB)
                    minB = t.X;
                if (t.X > maxB)
                    maxB = t.X;
            }

            return minA < maxB || minB < maxA;
        }

        public static CollisionShape Ellipse(float rX, float rY, Vector2 offset = default(Vector2), int numPoints = 8)
        {
            Vector2[] points = new Vector2[numPoints];

            for (int i = 0; i < numPoints; i++)
                points[i] = offset + new Vector2(
                    (float)Math.Cos(i * Math.PI * 2.0 / numPoints) * rX,
                    (float)Math.Sin(i * Math.PI * 2.0 / numPoints) * rY);

            return new CollisionShape(points);
        }
    }
}
